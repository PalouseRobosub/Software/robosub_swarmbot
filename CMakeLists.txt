cmake_minimum_required(VERSION 3.5)
project(robosub_swarmbot)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "-Wall -Werror -std=c++14 ${CMAKE_CXX_FLAGS}")
set(FastRTPS_INCLUDE_DIR /opt/ros/`rosversion -d`/include)
set(FastRTPS_LIBRARY_RELEASE /opt/ros/`rosversion -d`/lib/libfastrtps.so)

#find required components
find_package(rosidl_default_generators REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(std_msgs REQUIRED)
find_package(builtin_interfaces REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclpy REQUIRED)

################################################
## Declare ROS messages, services and actions ##
################################################

rosidl_generate_interfaces(${PROJECT_NAME}
  ${msg_files}
  DEPENDENCIES geometry_msgs std_msgs builtin_interfaces
)

ament_export_dependencies(rosidl_default_runtime rclcpp rclpy)

ament_package()
